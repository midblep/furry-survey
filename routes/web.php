<?php

use App\Http\Controllers\LocaleController;
use App\Http\Controllers\SurveyController;
use App\Http\Controllers\SetLocaleController;
use App\Models\SurveyEntry;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\App;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::localizedGroup(function () {
    Route::get('/', function() {
        return redirect()->route('index');
    });

    Route::get('/about', function() {
        return view('about');
    })->name('about');

    Route::get('/survey', [SurveyController::class, 'index'])->name('index');
    Route::post('/survey', [SurveyController::class, 'post']);
    Route::get('/sent', [SurveyController::class, 'sent'])->name('sent');
    Route::post('/setlocale', [SetLocaleController::class, 'post'])->name('setlocale');

    Route::get('/results', function() {
        $amount_total = \App\Models\SurveyEntry::count();
        $amount_furry = \App\Models\SurveyEntry::where('furry_bool', 1)->count();
        $amount_convention = SurveyEntry::where('attended_convention', 1)->count();
        $amount_furry_convention = SurveyEntry::where('furry_bool', 1)->where('attended_convention', 1)->count();
        $perc_convention = (($amount_convention / $amount_total) * 100) . "%";


        return view('results', [
            'amount_total' => $amount_total,
            'amount_furry' => $amount_furry,
            'amount_convention' => $amount_convention,
            'amount_furry_convention' => $amount_furry_convention,
            'perc_convention' => $perc_convention,
        ]);
    })->name('results');
});
