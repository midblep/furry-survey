@extends('layouts.app')

@section('content')
    <h4 class="font-weight-bold">{{ __('lang.fill') }}</h4>
    <hr>
    <form action="{{ route('index') }}" method="post">
        @csrf
        <div class="form-group">
            <label for="furry_bool" class="form-check-label">{{ __('lang.furry_boolean') }}</label>
            <select class="form-control" id="furry_bool" name="furry_bool">
                <option name="furry_bool" value="">{{ __('lang.choose') }}</option>
                <option value="0" name="furry_bool">{{ __('lang.no') }}</option>
                <option value="1" name="furry_bool">{{ __('lang.yes') }}</option>
            </select>
            @error('furry_bool')
            <small id="furry_bool_error" class="form-text text-danger">{{ $message }}</small>
            @enderror
        </div>

        <div class="form-group">
            <label for="attended_convention" class="form-check-label">{{ __('lang.attended_convention') }}</label>
            <select class="form-control" id="attended_convention" name="attended_convention">
                <option name="attended_convention" value="">{{ __('lang.choose') }}</option>
                <option value="0" name="attended_convention">{{ __('lang.no') }}</option>
                <option value="1" name="attended_convention">{{ __('lang.yes') }}</option>
            </select>
            @error('attended_convention')
                <small id="attended_convention_error" class="form-text text-danger">{{ $message }}</small>
            @enderror
        </div>

        <div class="form-group">
            <label for="language_native">{{ __('lang.language_native') }}</label>
            <select class="form-control" id="language_native" name="language_native">
                <option name="language_native" value="">{{ __('lang.choose') }}</option>
                @foreach($language_list_selector AS $key => $label)
                <option class="small" name="language_native" value="{{ $label }}">{{ $label }}</option>
                @endforeach
            </select>
            @error('language_native')
                <small id="furry_bool_error" class="form-text text-danger">{{ $message }}</small>
            @enderror
        </div>

        <div class="form-group">
            <label for="language_most_used">{{ __('lang.language_most_used') }}</label>
            <select class="form-control" id="language_most_used" name="language_most_used">
                <option name="language_most_used" value="">{{ __('lang.choose') }}</option>
                @foreach($language_list_selector AS $key => $label)
                    <option class="small" name="language_most_used" value="{{ $label }}">{{ $label }}</option>
                @endforeach
            </select>
            @error('language_most_used')
                <small id="furry_bool_error" class="form-text text-danger">{{ $message }}</small>
            @enderror
        </div>

        <button type="submit" class="btn btn-primary btn-block">{{ __('lang.submit') }}</button>
    </form>
@endsection
