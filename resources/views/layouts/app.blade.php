<!DOCTYPE html>
<html lang="en">

    <head>
        <title>{{ __('lang.site_name') }}</title>
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <script src="https://unpkg.com/chart.js@2.9.3/dist/Chart.min.js"></script>
        <script src="https://unpkg.com/@chartisan/chartjs@^2.1.0/dist/chartisan_chartjs.umd.js"></script>

    </head>

    <body style="background-color: #f1f1f1;">
        <div class="d-flex flex-column">
            <div class="container-lg py-4">
                <div class="container-lg m-auto" style="max-width: 600px;">
                    <h1 class="font-weight-bold text-center mt-4">{{ __('lang.site_name') }}</h1>
                    <div class="bg-light p-5 my-4 rounded">
                        @yield('content')
                    </div>

                    <div class="w-100 d-flex flex-column align-items-center">
                        <h5 class="font-weight-bold">{{ __('lang.choose_locale') }}</h5>
                        <form action="{{ route('setlocale') }}" method="post">
                            @csrf
                            <div class="form-group">
                                <select class="form-control" id="language_select" name="language_select" onchange='this.form.submit()'>
                                    @foreach($language_list_selector AS $list_key => $list_value)
                                        @foreach($language_list_supported as $supported_key => $supported_value)
                                            @if($list_key == $supported_value && $list_key == $locale)
                                                <option value="{{ $list_key }}" name="language_select">{{ $list_value }}</option>
                                            @endif
                                        @endforeach
                                    @endforeach
                                    @foreach($language_list_selector AS $list_key => $list_value)
                                        @foreach($language_list_supported as $supported_key => $supported_value)
                                            @if($list_key == $supported_value && $list_key != $locale)
                                                <option value="{{ $list_key }}" name="language_select">{{ $list_value }}</option>
                                            @endif
                                        @endforeach
                                    @endforeach
                                </select>
                            </div>
                        </form>
                    </div>

                    <p class="text-muted text-center my-0">
                        <a href="{{ route('index') }}">{{ __('lang.take_survey') }}</a>
                        |
                        <a href="{{ route('about') }}">{{ __('lang.about_link') }}</a>
                        |
                        <a href="{{ route('results') }}">{{ __('lang.results_link') }}</a>
                    </p>
                </div>
            </div>
        </div>
    </body>

</html>
