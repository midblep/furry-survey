@extends('layouts.app')

@section('content')
    <div class="alert alert-warning">
        <i class="fas fa-exclamation-triangle"></i>
        {{ __('lang.page_en') }}
    </div>

    <h4 class="font-weight-bold">Thank you!</h4>
    <p>
        We have received and saved your answers.
        Don't forget to share this survey with your friends, it will help make this survey's findings more accurate.
        If you want to know more about this survey, you can visit the about <a href="{{ route('about') }}">here</a>.
        Or you can visit the current ongoing survey results <a href="{{ route('results') }}">here</a>.
    </p>
@endsection
