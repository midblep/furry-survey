@extends('layouts.app')

@section('content')
    <div class="alert alert-warning">
        <i class="fas fa-exclamation-triangle"></i>
        {{ __('lang.page_en') }}
    </div>

    <h4 class="font-weight-bold">You have already filled in this survey before.</h4>
    <p>
        We have already received and saved your answers, thank you!
        Don't forget to share this survey with your friends, it will help make this survey's findings more accurate.
        If you want to know more about this survey, you can visit the about <a href="{{ route('about') }}">here</a>.
        Or you can visit the current ongoing survey results <a href="{{ route('results') }}">here</a>.
    </p>
@endsection
