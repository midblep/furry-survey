@extends('layouts.app')

@section('content')
    <div class="alert alert-warning">
        <i class="fas fa-exclamation-triangle"></i>
        {{ __('lang.page_en') }}
    </div>

    <h4 class="font-weight-bold">Survey Results <span class="badge badge-pill badge-danger"><i class="fas fa-exclamation-triangle"></i> not final</span></h4>

    <hr>

    <h5>Statistics overview:</h5>
    <p>
        Total survey participants: {{ $amount_total }}
        <br>
        Amount of furry participants: {{ $amount_furry }}
        <br>
        Total participants who attended conventions: {{ $amount_convention }}
        <br>
        Amount of furries who attended conventions: {{ $amount_furry_convention }}
        <br>
        Percentage of all participants who attended conventions: {{ $perc_convention }}
    </p>

    <hr>

    <h5>Questions overview:</h5>
    <div class="row">
        <div id="furry_boolean_chart" class="col-md-6" style="min-height: 25vh;"></div>
        <div id="attended_convention_chart" class="col-md-6" style="min-height: 25vh;"></div>
{{--        <span id="language_native" class="col-md-12" style="min-height: 25vh;"></span>--}}
{{--        <span id="language_most_used" class="col-md-12" style="min-height: 25vh;"></span>--}}
    </div>
    <script>
        const chart1 = new Chartisan({
            el: '#furry_boolean_chart',
            url: "@chart('furry_bool_chart')",
            hooks: new ChartisanHooks()
                .title('{{ __('lang.furry_boolean') }}')
                .datasets('pie')
                .pieColors(),
            options: {
                headers: {
                    'Request': 'test test',
                    'Test': 'test',
                }
            }
        });
        const chart2 = new Chartisan({
            el: '#attended_convention_chart',
            url: "@chart('furry_convention_chart')",
            hooks: new ChartisanHooks()
                .title('{{ __('lang.attended_convention') }}')
                .datasets('pie')
                .pieColors(),
            options: {
                headers: {
                    'Request': 'test test',
                    'Test': 'test',
                }
            }
        });
    </script>
    <p>More detailed charts coming soon</p>
@endsection
