<form action="{{ route('setlocale'}}" method="post">
    @csrf
    <div class="form-group">
        <select class="form-control" id="language_select" name="language_select" onchange='this.form.submit()'>
            @foreach($language_list_selector AS $list_key => $list_value)
                @foreach($language_list_supported as $supported_key => $supported_value)
                    @if($list_key == $supported_value)
                        <option value="{{ $list_key }}" name="language_select">{{ $list_value }}</option>
                    @endif
                @endforeach
            @endforeach
        </select>
    </div>
</form>
