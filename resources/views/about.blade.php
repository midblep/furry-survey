@extends('layouts.app')

@section('content')
    <div class="alert alert-warning">
        <i class="fas fa-exclamation-triangle"></i>
        {{ __('lang.page_en') }}
    </div>

    <h4 class="font-weight-bold">About</h4>
    <p>
        This survey has been created to gain more insight in the languages that the furry fandom uses the most.
        Filling in these questions is completely anonymous.
    </p>
    <p>
        This website has been built by Mid with Laravel and made possible by <a href="https://twitter.com/DubbelNull" target="_blank">DubbelNull</a>.
        It does not log any personal information other than the IP gathered from your browser.
    </p>
    <p>
        <a href="https://discord.gg/AY682SUb">Join the Discord server</a> to help us improve the survey's language translations or ask questions.
    </p>
@endsection
