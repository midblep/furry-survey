<?php
    return [
        'furry_boolean' => 'Ben je een Furry?',
        'attended_convention' => 'Ben je ooit naar een Furry conventie geweest?',
        'language_native' => 'Wat is je eerste taal?',
        'language_most_used' => 'Welke taal spreek je het vaakst?',
        'yes' => 'Ja',
        'no' => 'Nee',
        'submit' => 'Inleveren',
        'choose' => 'Kiezen...',
        'site_name' => 'Furry Talen Vragenlijst',
        'fill' => 'Vul alsjeblieft deze vragen in',
        'choose_locale' => 'Websitetaal',
        'thanks' => 'Bedankt!',
        'fill_more' => "Nog een invullen.",
    ]
?>
