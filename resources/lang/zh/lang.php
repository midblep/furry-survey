<?php
    return [ 
        'furry_boolean' => '你是Furry吗？| 你是Furry嗎？',
        'attended_convention' => '您去过Furry的聚会吗? | 您去過Furry的聚會嗎?',
        'language_native' => '什么是你的母语? | 什麼是你的母語?',
        'language_most_used' => '您最常使用哪种语言? | 您最常使用哪種語言？',
        'yes' => '是',
        'no' => '不',
        'submit' => '提交',
        'choose' => '选择 | 選擇',
    ]
?>