<?php
    return [ 
        'furry_boolean' => 'Êtes-vous un(e) Furry?',
        'attended_convention' => 'Avez-vous été à une convention Furry?',
        'language_native' => 'Quelle est votre langue maternelle?',
        'language_most_used' => 'Quelle langue utilisez-vous le plus?',
        'yes' => 'Oui',
        'no' => 'Non',
        'submit' => 'Présente',
        'choose' => 'Choisissez',
    ]
?>