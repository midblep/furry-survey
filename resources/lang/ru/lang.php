<?php
    return [ 
        'furry_boolean' => 'Вы Фурри?',
        'attended_convention' => 'Вы бывали на съездах Фурри?',
        'language_native' => 'Какой ваш родной язык?',
        'language_most_used' => 'Какой язык вы используете больше всего?',
        'yes' => 'Да',
        'no' => 'Нет',
        'submit' => 'Отправить',
        'choose' => 'Выберите ответ...',
        'site_name' => 'Языковой Опрос Фурри',
    ]
?>