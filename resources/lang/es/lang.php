<?php
    return [ 
        'furry_boolean' => '¿Eres un(a) Furry?',
        'attended_convention' => '¿Has estado en una convención de Furry?',
        'language_native' => '¿Cuál es tu idioma nativo?',
        'language_most_used' => '¿Qué idioma usas más?',
        'yes' => 'Si',
        'no' => 'No',
        'submit' => 'Presente',
        'choose' => 'Elija',
    ]
?>