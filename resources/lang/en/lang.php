<?php
    return [
        'furry_boolean' => 'Are you a Furry?',
        'attended_convention' => 'Have you been to a Furry convention?',
        'language_native' => 'What is your native language?',
        'language_most_used' => 'What language do you use the most?',
        'yes' => 'Yes',
        'no' => 'No',
        'submit' => 'Submit',
        'choose' => 'Choose...',
        'site_name' => 'Furry Language Survey',
        'choose_locale' => 'Site viewing language',
        'fill' => 'Please fill in these survey questions',
        'thanks' => 'Thank you!',
        'fill_more' => "Fill in another one.",
        'about_link' => "About this survey",
        'results_link' => "Survey results",
        'page_en' => "This page is only available in the English language, we apologize for the inconvenience.",
        'take_survey' => "Take survey",
    ]
?>
