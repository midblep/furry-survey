<?php
    return [ 
        'furry_boolean' => 'Você é um(a) Furry?',
        'attended_convention' => 'Você já foi a uma convenção Furry?',
        'language_native' => 'Qual é o seu idioma nativo?',
        'language_most_used' => 'Qual idioma você mais usa?',
        'yes' => 'Sim',
        'no' => 'Não',
        'submit' => 'Apresente',
        'choose' => 'Escolha',
    ]
?>