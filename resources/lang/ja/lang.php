<?php
    return [ 
        'furry_boolean' => 'あなたはFurryですか? ',
        'attended_convention' => 'Furry大会に行ったことはありますか? ',
        'language_native' => 'あなたの母国語は何ですか?',
        'language_most_used' => '一番よく使う言語は何ですか？',
        'yes' => 'ya',
        'no' => 'tidak',
        'submit' => '参加せよ',
        'choose' => '選べ',
    ]
?>