<?php
    return [ 
        'furry_boolean' => 'Adakah anda seorang Furry?',
        'attended_convention' => 'Anda pernah ke konvensyen Furry?',
        'language_native' => 'Apakah bahasa ibunda anda?',
        'language_most_used' => 'Bahasa apa yang paling anda gunakan?',
        'yes' => 'ya',
        'no' => 'tidak',
        'submit' => 'Menyerahkan',
        'choose' => 'Memilih',
    ]
?>