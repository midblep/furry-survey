<?php
    return [
        'furry_boolean' => 'Sind Sie ein(e) Furry?',
        'attended_convention' => 'Waren Sie schon auf einer Furry Convention?',
        'language_native' => 'Was ist Ihre muttersparche?',
        'language_most_used' => 'Welche Sprache verwenden Sie am häufigsten?',
        'yes' => 'Ja',
        'no' => 'Nein',
        'submit' => 'Einreichen',
        'choose' => 'Wähl',
    ]
?>
