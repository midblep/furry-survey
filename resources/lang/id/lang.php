<?php
    return [ 
        'furry_boolean' => 'Apakah Anda seorang Furry?',
        'attended_convention' => 'Anda pernah ke konvensi Furry?',
        'language_native' => 'Apa bahasa ibu kamu?',
        'language_most_used' => 'Bahasa apa yang paling sering kamu gunakan?',
        'yes' => 'ya',
        'no' => 'tidak',
        'submit' => 'Menyerahkan',
        'choose' => 'Memutuskan',
    ]
?>