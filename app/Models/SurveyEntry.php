<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SurveyEntry extends Model
{
    use HasFactory;

    protected $fillable = [
        'furry_bool',
        'attended_convention',
        'language_native',
        'language_most_used',
        'ip',
    ];
}
