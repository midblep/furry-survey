<?php

namespace App\Http\Controllers;

use App\Models\SurveyEntry;
use Arcanedev\Localization\Localization;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class SurveyController extends Controller
{
    public function compose(View $view) {
        $language_list_supported = ["en", "es", "fr", "id", "ja", "ms", "nl", "pt", "zh", "ru", "de"];
        $json_source = file_get_contents("data/languages.json");
        $json_decoded = json_decode($json_source, true);
        $language_list_iso = array();
        $language_list_selector = array();

        $counter = 0;
        foreach($json_decoded as $key => $item) {
            $subcounter = 0;
            $language_list_selector[$key] = $key;
            foreach($item as $subitem) {
                switch($subcounter) {
                    case 0:
                        $language_list_selector[$key] = $subitem;
                        $subcounter++;
                        break;
                    case 1:
                        $language_list_selector[$key] = $language_list_selector[$key] . " | " . $subitem;
                        $subcounter++;
                        break;
                }
            }
            $counter++;
        }

        $view
            ->with("language_list_iso", $language_list_iso)
            ->with("language_list_selector", $language_list_selector)
            ->with("language_list_supported", $language_list_supported)
            ->with("locale", App::currentLocale());
    }

    public function index() {
        return view('index');
    }

    public function sent(Localization $localization) {
        return view('sent');
    }

    public function post(Request $request, Localization $localization) {
        $this->validate($request, [
            'furry_bool' => 'required',
            'attended_convention' => 'required',
            'language_native' => 'required',
            'language_most_used' => 'required',
        ]);

        $ip = $request->ip();

        if (!DB::table('survey_entries')
            ->where('ip', '=', $ip)
            ->exists()) {
            SurveyEntry::create([
                'furry_bool' => $request->furry_bool,
                'attended_convention' => $request->attended_convention,
                'language_native' => $request->language_native,
                'language_most_used' => $request->language_most_used,
                'ip' => $request->ip(),
            ]);

            return redirect()->route('sent');
        } else {
            return view('denied');
        }
    }
}
