<?php

namespace App\Http\Controllers;

use Arcanedev\Localization\Contracts\Localization;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class SetLocaleController extends Controller
{
    //

    public function post(Request $request) {
        return redirect("/" . $request->input('language_select') . "/survey");
    }

}
