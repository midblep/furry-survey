<?php

declare(strict_types = 1);

namespace App\Charts;

use App\Models\SurveyEntry;
use Chartisan\PHP\Chartisan;
use ConsoleTVs\Charts\BaseChart;
use Illuminate\Http\Request;

class FurryConventionChart extends BaseChart
{
    /**
     * Handles the HTTP request for the given chart.
     * It must always return an instance of Chartisan
     * and never a string or an array.
     */
    public function handler(Request $request): Chartisan
    {
        $furry_bool_yes = SurveyEntry::where('attended_convention', 1)->count();
        $furry_bool_no = SurveyEntry::where('attended_convention', 0)->count();

        return Chartisan::build()
            ->labels([__('lang.yes'), __('lang.no')])
            ->dataset('Survey', [$furry_bool_yes, $furry_bool_no]);
    }
}
