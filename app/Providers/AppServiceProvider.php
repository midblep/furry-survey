<?php

namespace App\Providers;

use App\Http\Controllers\SurveyController;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use ConsoleTVs\Charts\Registrar as Charts;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Charts $charts)
    {
        $charts->register([
            \App\Charts\ResultsPieChart::class,
            \App\Charts\FurryBoolChart::class,
            \App\Charts\FurryConventionChart::class,
        ]);
    }
}
